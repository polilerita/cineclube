FROM mcr.microsoft.com/dotnet/core/sdk:3.1-alpine AS build-env
WORKDIR /app
RUN apk add git && \
    git clone https://gitlab.com/polilerita/cineclube.git

WORKDIR /app/cineclube/CineclubeApi

RUN dotnet restore

RUN dotnet publish -c Release -o /out

#Build runtime
FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-alpine
WORKDIR /app

COPY --from=build-env /out .
ENTRYPOINT ["dotnet", "CineclubeApi.dll"]